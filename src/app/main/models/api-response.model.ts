export class ApiResponse {
    'status': Object
    'data': Object
}
/*
 */
export class User {
    batch_id: any;
    class_id: any;
    session_id: any;
    roles_privleges: any;
    superAdminComponent: any;
    studentComponent: any;
    teacherComponent: any;
    student_id: any;
    curriculum_id: any;
    qp_id: any;
    parentComponent: any;
    admn_no: any;
    subjects: any;

    constructor(
        public academic_year: string,
        public allAcademic_year: any,
        public batchName: string,
        public className: string,
        public curriculumName: string,
        public employeeNo: string,
        public feature_privileges: any,
        public image: any,
        public module_privileges: any,
        public name: string,
        public quick_login_id: any,
        public role_ids: any,
        public school_id: string,
        public school_logo: string,
        public school_name: string,
        public sessionName: string,
        public user_id: string,
        public username: string,
        public admission_number: string,
        public _id: any
    ) { }
}

export interface Credentials {
    username?: string,
    password: string,
    email: string
}

export class Module {
    constructor(
        public id: string,
        public title: string,
        public url: string,
        public type: string = "item",
        public icon: string = "dashboard"
    ) { }
}
export class UserData {
    'email': string;
    'password': any;
    constructor(
        public id: string,
        public title: string,
        public url: string,
        public type: string = "item",
        public icon: string = "dashboard"
    ) { }
}
export class UserProperty {
    'email': string;
    'password': any;
    constructor(
        public academic_year: string,
        public addrLine1: any,
        public addrLine2: string,
        public admission_number: string,
        public batch: string,
        public batch_id: string,
        public city: any,
        public class_id: any,
        public user_id: any,
        public username: string,
    ) {

    }
}