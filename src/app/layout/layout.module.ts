import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import { MainCenteredComponent } from './main-centered/main-centered.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    MainLayoutComponent,
    MainHeaderComponent,
    MainCenteredComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([]),
  ],
  exports: [MainLayoutComponent, MainCenteredComponent, MainHeaderComponent]
})
export class LayoutModule { }
