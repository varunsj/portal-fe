import { Injectable } from '@angular/core';
import { NgbToastService, NgbToast, NgbToastType } from 'ngb-toast';

@Injectable({
    providedIn: 'root'
})
export class NgbtoastService {

    constructor(private toastService: NgbToastService) { }
    showSuccess(params: any = {}) {
        this.toastService.remove(params)
        const toast: NgbToast = {
            toastType: NgbToastType.Success,
            text: params.text,
            dismissible: true,
            timeInSeconds : 1,
            onDismiss: () => {
            }
        }
        this.toastService.show(toast);
    }
    showError(params: any = {}) {
        this.toastService.remove(params)
        const toast: NgbToast = {
            toastType: NgbToastType.Danger,
            text: params.text,
            dismissible: true,
            timeInSeconds : 1,
            onDismiss: () => {
            }
        }
        this.toastService.show(toast);
    }
    showWarning(params: any = {}) {
        this.toastService.remove(params)
        const toast: NgbToast = {
            toastType: NgbToastType.Warning,
            text: params.text,
            dismissible: true,
            timeInSeconds : 2,
            onDismiss: () => {
            }
        }
        this.toastService.show(toast);
    }
}
