import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutModule } from './layout/layout.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthModule } from './auth/auth.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DashboardModule } from './main/modules/dashboard/dashboard.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ApiInterceptor } from './interceptors/api.interceptor';
import { NgbToastModule } from 'ngb-toast';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule.withServerTransition({ appId: 'serverApp' }),
        AppRoutingModule,
        CommonModule,
        NgbModule,
        LayoutModule,
        AuthModule,
        ReactiveFormsModule,
        FormsModule,
        DashboardModule,
        NgbToastModule
    ],
    providers: [{
        provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true
    }],
    bootstrap: [AppComponent]
})
export class AppModule { }
