import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import {  AbstractControl,FormControl,FormBuilder, FormGroup,Validators } from '@angular/forms';
// import Validation from './utils/validation';


import { AuthService } from 'src/app/main/services/auth/auth.service';
import { NgbtoastService } from 'src/app/main/services/ngb-toast/ngbtoast.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
    
    loginForm = new FormGroup({
        email: new FormControl(''),
        password: new FormControl(''),
    });
    submitted = false;
    constructor(private router: Router, private authService : AuthService, 
        private formBuilder: FormBuilder,
        private toastr : NgbtoastService) { }

    ngOnInit(): void {
        this.loginForm = this.formBuilder.group({
            email:['', Validators.required],
            password:['', Validators.required],
        })


        this.authService.initialiseLogin()
            .subscribe(val => {
                if (val) {
                    this.router.navigate(['/dashboard'])
                }

            })
    }
    get f(): { [key: string]: AbstractControl } {
        return this.loginForm.controls;
      }
    
    onSubmit() {
        (this.authService.isLoggedIn)
        this.submitted = true;
        
        if (this.loginForm.valid) {
            this.authService
                .authenticate(this.loginForm.value)
                .subscribe(() => {
                    if (this.authService.isLoggedIn) {
                        this.showSuccess()
                        this.router.navigate(['/dashboard']);
                    }
                })
        }
    }
    showSuccess(): void {
        this.toastr.showSuccess({
            text: "Welcome..."
        });
    }
    ngOnDestroy(){
        
    }
   
    
}