import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { takeUntil } from 'rxjs/operators';
import { AuthService } from '../main/services/auth/auth.service';
@Injectable()
export class ApiInterceptor implements HttpInterceptor {
    headerToken: string = environment.headerToken
    SERVER_URL: string = environment.SERVER_URL
    API_VERSION: string = environment.API_VERSION
    constructor(private authService: AuthService) { }

    intercept<T>(request: HttpRequest<T>, next: HttpHandler): Observable<HttpEvent<T>> {
        let reqWithToken: any = {}
        let httpOptions = {
            headers: new HttpHeaders()
        }
        httpOptions.headers.append('strict-origin-when-cross-origin', '*');
        httpOptions.headers.append('Access-Control-Allow-Origin', '*');
        httpOptions.headers.append('Content-Type', 'application/json');
        httpOptions.headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        reqWithToken = request.clone({
            url: this.SERVER_URL + this.API_VERSION + '/' + request.url,
            setHeaders: {
                'Content-Type': 'application/json',
                "x-auth-token": this.headerToken
            },
        });
        return next.handle(reqWithToken).pipe(takeUntil(this.authService.onCancelPendingRequests()))
    }
}
