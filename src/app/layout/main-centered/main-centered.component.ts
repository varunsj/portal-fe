import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-centered',
  templateUrl: './main-centered.component.html',
  styleUrls: ['./main-centered.component.scss']
})
export class MainCenteredComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
