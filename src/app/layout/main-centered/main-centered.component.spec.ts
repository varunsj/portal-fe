import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainCenteredComponent } from './main-centered.component';

describe('MainCenteredComponent', () => {
  let component: MainCenteredComponent;
  let fixture: ComponentFixture<MainCenteredComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainCenteredComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainCenteredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
