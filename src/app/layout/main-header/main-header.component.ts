import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/main/services/auth/auth.service';
import { takeUntil } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
@Component({
    selector: 'app-main-header',
    templateUrl: './main-header.component.html',
    styleUrls: ['./main-header.component.scss']
})
export class MainHeaderComponent implements OnInit {
    destroy_one: any = Subscription;
    private _unsubscribeAll: Subject<any>;
    myValueSub: any = Subscription;
    user_obj : any = {}
    constructor(private auth: AuthService) {
        this._unsubscribeAll = new Subject();
        this.getUserDetails()
    }
    private getUserDetails(): any {
        let self = this
        this.myValueSub = this.auth.userObj
            .pipe(takeUntil(self._unsubscribeAll))
            .subscribe(data => {
                if (data) {
                    this.user_obj = data
                }
            })
    }
    ngOnInit(): void {
        console.log(this.user_obj)
    }
    logout() {
        this.auth.logout()
    }
}
