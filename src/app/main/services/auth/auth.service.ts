import { Injectable } from '@angular/core';
import {
    BehaviorSubject, Observable,
    throwError,
    Subject,
    ReplaySubject,
    Subscription
} from "rxjs";
import { Credentials, ApiResponse, User, UserData, UserProperty } from 'src/app/main/models/api-response.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {
    tap,
    catchError,
    take,
    map
} from 'rxjs/operators';
import { Router,ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';

declare var $: any;
import { Observer, fromEvent, merge } from 'rxjs';
// import { NgbtoastService } from '../ngbtoast/ngbtoast.service';
import * as _ from 'underscore';
import { NgbtoastService } from '../ngb-toast/ngbtoast.service';
const _LOCAL_NAME = 'userData'
export class gAuthResponse {
    'status': Object
    'data' : any = {
        data : []
    }
}
@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private gloginUrl: string = 'gsuit_login_auth'
    private loginUrl: string = 'login_auth'
    private pendingHTTPRequests$ = new Subject<void>();
    public isLoggedIn: Boolean = false
    public userObj: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    ENV: any=environment
    
    constructor(public httpClient: HttpClient, private router: Router, 
        private toastService : NgbtoastService
        ) {
        let storedProp = localStorage.getItem(_LOCAL_NAME);
        if (storedProp) {
            this.setProperty(JSON.parse(storedProp), true);
        }
    }
    public cancelPendingRequests() {
        this.pendingHTTPRequests$.next();
    }

    public onCancelPendingRequests() {
        return this.pendingHTTPRequests$.asObservable();
    }
    setProperty(property: UserProperty, storeProp: boolean = false) {
        if (storeProp) {
            localStorage.setItem(_LOCAL_NAME, JSON.stringify(property));
        }
        this.userObj.next(property)
    }
    private hasToken(): boolean {
        return !!localStorage.getItem(_LOCAL_NAME);
    }
    isLogged() {
        return this.hasToken()
    }
    authenticate(credentials: Credentials): Observable<ApiResponse> {
        credentials.username = credentials.email
        return this.httpClient
            .post<ApiResponse>(this.loginUrl, credentials)
            .pipe(
                tap((apiResponse: any) => {
                  console.log(apiResponse)
                    this.handleAuthentication(
                      
                        apiResponse.data.details
                    );
                }),
                catchError(this.handleError<any>("Authentication"))
            );
    }
    private handleAuthentication(responseUserData: any) {
        let user = responseUserData;
        this.isLoggedIn = true
        this.setProperty(user, false);
        localStorage.setItem(_LOCAL_NAME, JSON.stringify(user));

    }
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            
            let errorMessage = error
            console.log(error)
            if (error.error && error.error.error && error.error.error){
                
                errorMessage = error.error.error.error
            } 
            console.log(errorMessage)
            this.toastService.showError({
                text:errorMessage
            });
            return throwError(error);
        };
    }
    showSuccess(): void {
        
    }
    logout() {
        localStorage.clear();
        this.userObj.next(null);
        this.router.navigate(['login']);
        
    }
    _apiCallPostMethod(obj: Object, url: string): Object {
        return new Promise((resolve, reject) => {
            return this.httpClient.post(url, obj).subscribe(
                data => {
                    resolve(data)
                },
                error => {
                    resolve(error)
                }
            );
        })
    }
    _apiCallGetMethod(url: string): Object {
        return new Promise((resolve, reject) => {
            return this.httpClient.get(url).subscribe(
                data => {
                    resolve(data)
                },
                error => {
                    resolve(error)
                }
            );
        })
    }
    initialiseLogin(): Observable<boolean> {
        return this.userObj.pipe(
            take(1),
            map(user => {
                const isAuth = !!user;
                if (isAuth) {
                    return true;
                }
                return false;
            })
        );
    }
    public reportcode: BehaviorSubject<any> = new BehaviorSubject<any[]>([]);
    setCode(): void{
    let self = this
    let url: any = this.router.url.split('/')
    if (url && url.length) {
      let fet_id = url[url.length - 1]
      let RP_LOCALSTORE: any = localStorage.getItem(self.ENV.RP_LOCAL);

      if (RP_LOCALSTORE) {
        let rpLocal: any = JSON.parse(RP_LOCALSTORE);
        if (rpLocal && fet_id) {
          let ex = _.find(rpLocal, function (one) {
            return one._id == fet_id
          })
          if(ex){
            console.log(ex.code, fet_id)
            let reportcode=ex.code
            this.reportcode.next(ex.code)
            console.log(reportcode)
          }
          
        }
      }
    }
    }
    _checkIsCurrentUser(info : any = {}){
        let ret_val = false
        try{
            let RP_LOCALSTORE: any = localStorage.getItem(_LOCAL_NAME);
            if(info && info.email){
                if(this.hasToken()){
                    let trimmedEmail = info.email.trim()
                    let currentUser : any = JSON.parse(RP_LOCALSTORE);
                    if(currentUser && currentUser.user_id){
                        console.log(currentUser)
                        if(currentUser.username == trimmedEmail){
                            ret_val = true
                        }
                    }
                }
            }
            return ret_val
        }catch(e){
            console.log(e)
            return false
        }
    }
    logoutForAutoLogin() {
        this.isLoggedIn = false
        localStorage.clear();
        this.userObj.next(null);
    }
    googleAuthenticate(credentials: any = {}): Observable<gAuthResponse> {
        credentials.username = credentials.email
        return this.httpClient
            .post<gAuthResponse>(this.gloginUrl, credentials)
            .pipe(
                tap((apiResponse: any) => {
                    if (apiResponse.data &&
                        apiResponse.data.data) {
                        this.handleGoogleAuthentication(
                            apiResponse.data.data[0]
                        );
                    } else {
                        this.handleResponseError(apiResponse)
                    }
                }),
                catchError(this.handleError<any>("Authentication"))
            );
    }
    private handleGoogleAuthentication(responseUserData: any) {
        try{
            let user = responseUserData;
            this.isLoggedIn = true
            this.setProperty(user, false);
            localStorage.setItem(_LOCAL_NAME, JSON.stringify(user))
        }catch(e){
            console.log(e)
        }
    }
    private handleResponseError(apiResponse: any): Observable<any> {
        try {
            let errorMessage = "Error occured"
            if (apiResponse.error) {
                if (apiResponse.error.errors) {
                    if (apiResponse.error.errors.password &&
                        apiResponse.error.errors.password.msg) {
                        errorMessage = "Invalid password"
                    }
                    if (apiResponse.error.errors.username &&
                        apiResponse.error.errors.username.msg) {
                        errorMessage = "Invalid username"
                    }
                    if (apiResponse.error.errors.message && 
                        apiResponse.error.errors.message == "Invalid user role"){
                            errorMessage = "Unauthorised user"
                    }
                }
                if (apiResponse.error.message) {
                    errorMessage = apiResponse.error.message
                }
            }
            return throwError(apiResponse.error);
        } catch (e) {
            console.log(e)
            return throwError(e);
        }
    }
}






