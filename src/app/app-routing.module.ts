
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import { AuthGuard } from './guards/auth.guard';
import { MainCenteredComponent } from './layout/main-centered/main-centered.component';
const routes: Routes = [
    {
        path: '',
        component: MainCenteredComponent,
        children: [
            { path: 'login', component: LoginComponent },
            { path: '', component: LoginComponent }
        ],
    },
    {
        path: 'dashboard',
        canActivate: [AuthGuard],
        component: MainLayoutComponent,
        loadChildren: () =>
            import('./main/modules/dashboard/dashboard.module').then((m) => m.DashboardModule),
    },
    {
        path: '**',
        component: MainCenteredComponent,
        children: [
            { path: '**', component: NotfoundComponent }
        ],
    },];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        initialNavigation: 'enabled'
    })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
