import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/main/services/auth/auth.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  constructor(private  auth : AuthService ) { }

  ngOnInit(): void {
  }
  logout(){
this.auth.logout()
  }
}
